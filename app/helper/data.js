export const showPage = [
  {
    label: 10,
    value: 10,
  },
  {
    label: 20,
    value: 20,
  },
  {
    label: 50,
    value: 50,
  },
]
export const sortBy = [
  {
    label: 'Newest',
    value: '-published_at',
  },
  {
    label: 'Oldest',
    value: 'published_at',
  },
]