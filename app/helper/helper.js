import { parseISO } from 'date-fns';

export const formattedDate = (rawDate) => {
  const parsedDate = parseISO(rawDate);
  const utc = new Date(parsedDate);
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  };
  const formattedDate = utc.toLocaleDateString('ID-id', options);
  return formattedDate;
};
