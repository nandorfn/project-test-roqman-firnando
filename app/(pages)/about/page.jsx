import { Navbar } from "@/app/components/Navbar";

const Page = () => {
  return (
    <Navbar>
      <div className="flex m-auto text-black h-screen justify-center items-center">
        <h1 className=" text-4xl">About</h1>
      </div>
    </Navbar>
  )
}

export default Page;
