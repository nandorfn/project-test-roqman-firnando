'use client'
import { ArticleContainer } from "@/app/components/Container/ArticleContainer"
import { Navbar } from "@/app/components/Navbar"
import { Parallax, ParallaxLayer } from "@react-spring/parallax"
import { useSearchParams } from "next/navigation"
import { useEffect, useState } from "react"

const Page = () => {
  const [limitPage, setLimitPage] = useState();
  const searchParams = useSearchParams();
  const limit = searchParams.get('limit');

  useEffect(() => {
    if (limit === '50') setLimitPage(9);
    if (limit === '20') setLimitPage(3);
    if (limit === '10' || !limit) setLimitPage(2);
  }, [limit])
      
  return (
    <Parallax pages={limitPage}>
      <Navbar />
      <ParallaxLayer
        speed={0.6}
        factor={1}
        className="clip-path"
        style={{
          backgroundImage: `url('https://suitmedia.static-assets.id/storage/files/601/6.jpg')`,
          backgroundSize: 'cover',
          height: '600px'
        }}
      />

      <ParallaxLayer speed={0.2} offset={0.2}>
        <figure className={`flex flex-col justify-center items-center  text-white `}>
          <h1 className=' text-7xl font-medium'>Ideas</h1>
          <p className=' font-medium text-xl'>Where all our great things begin</p>
        </figure>
      </ParallaxLayer>

      <ParallaxLayer speed={0.5} factor={2} offset={0.7} className=" h-fit">
        <ArticleContainer />
      </ParallaxLayer>
    </Parallax>
  )
}

export default Page;
