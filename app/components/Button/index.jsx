import React from 'react'

export const Button = ({ children, ...props }) => {
  return (
    <button className='disabled:opacity-50' {...props}>{children}</button>
  )
}
