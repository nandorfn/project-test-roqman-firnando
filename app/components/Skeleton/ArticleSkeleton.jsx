import React from 'react';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

export const ArticleSkeleton = ({ limit }) => {
  const dummyArray = Array.from({ length: limit }, (_, index) => index);

  return (
    <div className='grid gap-3 lg:gap-5 grid-cols-2 md:grid-cols-3 xl:grid-cols-4 overflow-y-scroll p-4'>
      {dummyArray.map((index) => (
        <React.Fragment key={index}>
          <ArticleItemSkeleton />
        </React.Fragment>
      ))}
    </div>
  );
};

const ArticleItemSkeleton = () => {
  return (
    <article className='shadow-lg min-h-full flex flex-col  rounded-xl'>
      <Skeleton
        height={200}
        className='rounded-t-md h-[60%]'
        alt="thumbnail"
      />
      <div className=' p-4'>
        <Skeleton width={100} />
        <Skeleton count={3} />
      </div>
    </article>
  );
};
