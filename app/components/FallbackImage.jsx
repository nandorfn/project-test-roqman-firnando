import { useEffect, useState } from 'react'
import Image from 'next/image'
import noImage from '../assets/no-photo-available.png'

export const FallbackImage = ({ src, ...rest }) => {
  const [imgSrc, setImgSrc] = useState(src)

  useEffect(() => {
    setImgSrc(src)
  }, [src])

  return (
    <Image
      {...rest}
      src={imgSrc || noImage}
      onError={() => {
        setImgSrc('/images/not-found.png')
      }}
    />
  )
}