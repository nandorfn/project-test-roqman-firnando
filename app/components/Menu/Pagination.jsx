import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useCallback } from 'react';
import { Button } from '../Button';

const Pagination = ({ currentPage, totalPages }) => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  const createQueryString = useCallback(
    (name, value) => {
      const params = new URLSearchParams(searchParams);
      params.set(name, value);
      return params.toString();
    },
    [searchParams]
  );

  const handleButtonClick = (pageNumber) => (event) => {
    event.preventDefault();
    router.replace(pathname + '?' + createQueryString('page', pageNumber));
  };

  const startPage = Math.max(1, currentPage - 1);
  const endPage = Math.min(totalPages, startPage + 3);
  const pageNumbers = Array.from({ length: endPage - startPage + 1 }, (_, index) => startPage + index);

  return (
    <div className="font-semibold inline-flex gap-4 mx-auto mt-10 mb-32">
      <Button onClick={handleButtonClick(1)} disabled={currentPage === 1}>&lt;&lt;</Button>
      <Button onClick={handleButtonClick(Number(currentPage) - 1)} disabled={currentPage === 1}>&lt;</Button>
      {pageNumbers.map((number) => (
        <Button
          key={number}
          className={`text-sm ${currentPage === number ? 'bg-orange-400 px-2 py-1 text-white rounded-md' : ''}`}
          onClick={handleButtonClick(number)}
        >
          {number}
        </Button>
      ))}
      <Button onClick={handleButtonClick(parseInt(currentPage) + 1)} disabled={currentPage === totalPages}>&gt;</Button>
      <Button onClick={handleButtonClick(totalPages)} disabled={currentPage === totalPages}>&gt;&gt;</Button>
    </div>
  );
};

export default Pagination;
