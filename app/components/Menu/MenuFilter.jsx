'use client'
import { usePathname, useSearchParams, useRouter } from "next/navigation"
import { Select } from "../Form/Select"
import { useCallback } from "react"
import { showPage, sortBy } from "@/app/helper/data"

export const MenuFilter = ({ data }) => {
  const router = useRouter()
  const pathname = usePathname()
  const searchParams = useSearchParams()

  const limit = searchParams.get('limit')
  const sort = searchParams.get('sort')

  const createQueryString = useCallback(
    (name, value) => {
      const params = new URLSearchParams(searchParams)
      params.set(name, value)

      return params.toString()
    },
    [searchParams]
  )

  const handleInput = (e) => {
    const { name, value } = e.target
    router.push(pathname + `?` + createQueryString(name, value))
  }
  
  return (
    <div className="flex flex-col-reverse gap-4 md:gap-0 md:flex-row justify-between md:items-center px-4">
      <p>{`Showing ${data?.from ?? 1} - ${data?.to ?? 10} of ${data?.last_page ?? 100}`}</p>

      <div className=" inline-flex justify-between gap-5">
        <label className="flex flex-row items-center gap-4" htmlFor="limit">
          Show per page
          <Select
            name={'limit'}
            value={limit}
            handleChange={handleInput}
            setDefault={false}
            options={showPage} />
        </label>
        <label className="flex flex-row items-center gap-4" htmlFor="limit">
          Sort by
          <Select
            name={'sort'}
            value={sort}
            handleChange={handleInput}
            setDefault={false}
            options={sortBy} />
        </label>
      </div>
    </div>
  )
}
