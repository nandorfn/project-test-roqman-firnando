import React from 'react'

export const Select = ({
  options,
  name,
  handleChange,
  value,
  className,
  setDefault,
  ...props
}) => {
  return (
    <select
      {...props}
      name={name}
      onChange={handleChange}
      value={value}
      className='p-2 w-22 md:w-28 border rounded-full bg-transparent'
    >
      {setDefault &&
        <option selected="">Choose</option>
      }
      {options?.map((item, index) => (
        <option
          key={index}
          value={item.value}
        >
          {item.label}
        </option>
      ))
      }
    </select>

  )

}