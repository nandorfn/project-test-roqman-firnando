"use client"
import { useQuery } from '@tanstack/react-query'
import React from 'react'
import Pagination from '../Menu/Pagination'
import client from '@/app/libs/axios'
import { useSearchParams } from 'next/navigation'
import { ArticleItem } from './ArticleItem'
import { MenuFilter } from '../Menu/MenuFilter'
import { ArticleSkeleton } from '../Skeleton/ArticleSkeleton'

export const ArticleContainer = () => {
  const searchParams = useSearchParams()
  const page = searchParams.get('page') || 1;
  const limit = searchParams.get('limit') || 10;
  const sort = searchParams.get('sort') || '-published_at'

  const fetchArticles = async (page = 1) => {
    try {
      const res = client.get(`/ideas?page[number]=${page}&page[size]=${limit}&append[]=small_image&append[]=medium_image&sort=${sort}`)
      return res;
    } catch (error) {
      console.error(error.message)
    }
  }

  const {
    isPending,
    isError,
    data,
    isFetching,
  } = useQuery({
    queryKey: ['articles', page, limit, sort],
    queryFn: () => fetchArticles(page),
  })
  
  const totalPages = Math.ceil(Number(data?.data?.meta?.total) / Number(data?.data?.meta?.per_page));
  
  return (
    <section className='flex flex-col gap-4 max-w-7xl mx-auto'>
      <MenuFilter data={data?.data?.meta} />
      
      {isError ? (
      <div className='max-w-7xl mx-auto'>
        <p className='text-center'>Gagal memuat data artikel!</p>
      </div>
    ) : (
      <>
        {isPending || isFetching ? (
          <ArticleSkeleton limit={limit} />
        ) : (
          <div className='grid gap-3 lg:gap-5 grid-cols-2 md:grid-cols-3 xl:grid-cols-4 p-4'>
            {data?.data?.data?.map((item) => (
              <React.Fragment key={item?.id}>
                <ArticleItem data={item} />
              </React.Fragment>
            ))}
          </div>
        )}
      </>
    )}
      
        <Pagination currentPage={Number(page)} totalPages={totalPages ?? 100} />
    </section>
  )
}
