import React from 'react'
import { formattedDate } from '@/app/helper/helper'
import { FallbackImage } from '../FallbackImage'

export const ArticleItem = ({ data }) => {
  const date = formattedDate(data?.published_at)
  return (
    <article className='shadow-lg min-h-full flex flex-col w-fit rounded-xl'>
      <FallbackImage
        className='rounded-t-md h-[60%]'
        src={data?.medium_image[0]?.url}
        width={500}
        height={500}
        alt="thumbnail" />
      <div className=' p-4'>
        <p className=' font-light'>{date.toUpperCase()}</p>
        <p className=' font-semibold line-clamp-3'>{data?.title}</p>
      </div>
    </article>
  )
}
