'use client'
import Image from 'next/image'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import logo from '../assets/logo-white.081d3ce.png'

export const Navbar = ({ children }) => {
  const pathname = usePathname()
  const menus = [
    { label: 'Home', link: '/' },
    { label: 'About', link: '/about' },
    { label: 'Services', link: '/services' },
    { label: 'Ideas', link: '/ideas' },
    { label: 'Careers', link: '/careers' },
    { label: 'Contacts', link: '/contacts' },
  ]
  

  const navLink = menus.map((menu, index) => (
    <li key={index} className={` ${pathname === menu.link ? 'border-b-4' : ''}`}>
      <Link href={menu.link}>
        {menu.label}
      </Link>
    </li>
  ))
  
  return (
    <>
    <header className={`fixed top-0 bg-orange-500 w-full z-50`}>
      <nav className='flex flex-row justify-between p-4 max-w-7xl mx-auto'>
        <Image
          src={logo}
          width={125}
          height={50}
          alt="suitmedia" />
        <ul className='inline-flex gap-5 items-center text-white'>
          {navLink}
        </ul>
      </nav>
    </header>
    <main>
    {children}
    </main>
    </>
  )
}
