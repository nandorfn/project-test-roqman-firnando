import { Navbar } from "./components/Navbar";

export default function Home() {
  return (
    <>
      <Navbar>
        <div className="flex m-auto text-black h-screen justify-center items-center">
          <h1 className=" text-4xl">Home</h1>
        </div>
      </Navbar>
    </>
  )
}
